/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfaceTrashMonitorWorkArea;

import Business.Organization1.Organization;
import Business.Organization1.TrashMonitorOrganization;
import java.util.Stack;

/**
 *
 * @author sriman
 */
public class TSPNearestNeighbour {
    private int numberOfNodes;
    private Stack<Integer> stack;
    private TrashMonitorOrganization trashmonitororganization;
 
    public TSPNearestNeighbour(Organization organization)
    {
        stack = new Stack<Integer>();
        this.trashmonitororganization=(TrashMonitorOrganization)organization;
        
    }
 
    public void tsp(int adjacencyMatrix[][])
    {
        numberOfNodes = adjacencyMatrix[1].length - 1;
        int[] visited = new int[numberOfNodes + 1];
        visited[1] = 1;
        stack.push(1);
        int element, dst = 0, i;
        int min = Integer.MAX_VALUE;
        boolean minFlag = false;
        
        System.out.print(1 + "\t");
 
        while (!stack.isEmpty())
        {
            element = stack.peek();
            i = 1;
            min = Integer.MAX_VALUE;
            while (i <= numberOfNodes)
            {
                if (adjacencyMatrix[element][i] > 1 && visited[i] == 0)
                {
                    if (min > adjacencyMatrix[element][i])
                    {
                        min = adjacencyMatrix[element][i];
                        dst = i;
                        minFlag = true;
                    }
                }
                i++;
            }
            if (minFlag)
            {
                visited[dst] = 1;
                stack.push(dst);
                trashmonitororganization.getSortedtc().add(dst);
                System.out.println("the element is: "+dst);
                System.out.print(dst + "\t");
                minFlag = false;
                continue;
            }
            stack.pop();
        }
        
        System.out.println("The sorted size is: "+trashmonitororganization.getSortedtc().size());
    
}
}
