/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue1;

/**
 *
 * @author sriman
 */


import Business.Driver.Driver;
import Business.Employee1.Employee;

import java.util.ArrayList;

public class DriverWorkRequest
  extends WorkRequest
{
  private String testResult;
  private String path;
  private Driver d; // driver object to be sent

    public Driver getD() {
        return d;
    }

    public void setD(Driver d) {
        this.d = d;
    }
  
  public String getTestResult()
  {
    return this.testResult;
  }
  
  public void setTestResult(String testResult)
  {
    this.testResult = testResult;
  }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
  
  
 
}

