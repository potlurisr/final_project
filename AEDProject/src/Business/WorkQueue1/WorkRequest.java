/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue1;

/**
 *
 * @author sriman
 */


import Business.UserAccount1.UserAccount;
import java.util.ArrayList;
import java.util.Date;

public abstract class WorkRequest
{
  private String message;
  private UserAccount sender;
  private UserAccount receiver;
  private String status;
  private Date requestDate;
  private Date resolveDate;
  private ArrayList<Integer> path;//to store the path
  
  public WorkRequest()
  {
    this.requestDate = new Date();
    path=new ArrayList<>();
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  public void setMessage(String message)
  {
    this.message = message;
  }
  
  public UserAccount getSender()
  {
    return this.sender;
  }
  
  public void setSender(UserAccount sender)
  {
    this.sender = sender;
  }
  
  public UserAccount getReceiver()
  {
    return this.receiver;
  }
  
  public void setReceiver(UserAccount receiver)
  {
    this.receiver = receiver;
  }
  
  public String getStatus()
  {
    return this.status;
  }
  
  public void setStatus(String status)
  {
    this.status = status;
  }
  
  public Date getRequestDate()
  {
    return this.requestDate;
  }
  
  public void setRequestDate(Date requestDate)
  {
    this.requestDate = requestDate;
  }
  
  public Date getResolveDate()
  {
    return this.resolveDate;
  }
  
  public void setResolveDate(Date resolveDate)
  {
    this.resolveDate = resolveDate;
  }
}
