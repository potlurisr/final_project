/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Technician;

import java.util.ArrayList;

/**
 *
 * @author sriman
 */
public class TechnicianDirectory {
    
    private ArrayList<Technician> dd;
    
    public TechnicianDirectory()
    {
        dd=new ArrayList<>();
    }
    
    public Technician addTechnician()
    {
        Technician bd=new Technician();
        dd.add(bd);
        return bd;
    }

    public ArrayList<Technician> getdd() {
        return dd;
    }
    
}
