/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Driver;

/**
 *
 * @author sriman
 */
public class Driver {
    
    private String name;
    private int id;
    private int age;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    private String liscence_no;
    private String validity;
    private static int counter;

    public Driver()
    {
        id=counter;
        counter++;
        
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLiscence_no() {
        return liscence_no;
    }

    public void setLiscence_no(String liscence_no) {
        this.liscence_no = liscence_no;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }
    
    @Override
    public String toString()
    {
        return Integer.toString(id);
        
    }
    
}
