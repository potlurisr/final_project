/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Business;

import Business.Employee1.Employee;
import Business.Role1.SystemAdminRole;
import Business.UserAccount1.UserAccount;

/**
 *
 * @author sriman
 */


public class ConfigureASystem
{
  public static EcoSystem configure()
  {
    EcoSystem system = EcoSystem.getInstance();
    
    
     Employee employee = system.getEmployeeDirectory().createEmployee("sysadmin");
        
        UserAccount ua = system.getUserAccountDirectory().createUserAccount("sysadmin", "sysadmin", employee, new SystemAdminRole());
    
    return system;
  }
}

