/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise1;

/**
 *
 * @author sriman
 */


import java.util.ArrayList;

public class EnterpriseDirectory
{
  private ArrayList<Enterprise> enterpriseList;
  
  public EnterpriseDirectory()
  {
    this.enterpriseList = new ArrayList();
  }
  
  public ArrayList<Enterprise> getEnterpriseList()
  {
    return this.enterpriseList;
  }
  
  public Enterprise createAndAddEnterprise(String name, Enterprise.EnterpriseType type)
  {
    Enterprise enterprise = null;
    if (type == Enterprise.EnterpriseType.Waste)
    {
      enterprise = new WasteManagementEnterprise(name);
      this.enterpriseList.add(enterprise);
    }
    return enterprise;
  }
}

