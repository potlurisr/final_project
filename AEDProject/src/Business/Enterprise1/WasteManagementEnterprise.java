/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise1;

/**
 *
 * @author sriman
 */

import Business.Role1.Role;
import java.util.ArrayList;

public class WasteManagementEnterprise extends Enterprise {
    
  public WasteManagementEnterprise(String name)
  {
    super(name, Enterprise.EnterpriseType.Waste);
  }
  
  public ArrayList<Role> getSupportedRole()
  {
    return this.roles;
  }
}

    

