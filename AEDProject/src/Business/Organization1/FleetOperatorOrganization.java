/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization1;
import Business.Role1.FleetOperatorRole;
import Business.Role1.Role;
import Business.Vehicle.Vehicle;
import java.util.ArrayList;

/**
 *
 * @author sriman
 */
public class FleetOperatorOrganization extends Organization {
    
   private ArrayList<Vehicle> v;
    

  public FleetOperatorOrganization()
  {
    super(Organization.Type.FleetOperator.getValue());
    v=new ArrayList<Vehicle>();
    
  }

    public ArrayList<Vehicle> getV() {
        return v;
    }

    public void setV(ArrayList<Vehicle> v) {
        this.v = v;
    }
  
  public ArrayList<Role> getSupportedRole()
  {
      roles.add(new FleetOperatorRole());
    return roles;
  }
}

    

