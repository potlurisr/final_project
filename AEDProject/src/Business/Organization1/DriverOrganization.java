/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization1;

import Business.Driver.DriverDirectory;
import Business.Role1.DriverRole;
import Business.Role1.Role;
import java.util.ArrayList;

/**
 *
 * @author sriman
 */
public class DriverOrganization extends Organization {
    
   
    private DriverDirectory dd;
     public DriverOrganization()
  {
    super(Organization.Type.Driver.getValue());
    dd=new DriverDirectory();
  }
  
  public ArrayList<Role> getSupportedRole()
  {
      roles.add(new DriverRole());
    return roles;
  }

    public DriverDirectory getDd() {
        return dd;
    }

    public void setDd(DriverDirectory dd) {
        this.dd = dd;
    }
    
    
}
