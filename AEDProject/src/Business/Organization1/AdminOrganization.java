/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization1;


import Business.Role1.AdminRole;
import Business.Role1.Role;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class AdminOrganization extends Organization{

    public AdminOrganization() {
        super(Type.Admin.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new AdminRole());
        return roles;
    }
     
}
