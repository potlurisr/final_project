/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization1;

/**
 *
 * @author sriman
 */


import java.util.ArrayList;

public class OrganizationDirectory
{
  private ArrayList<Organization> organizationList;
  
  public OrganizationDirectory()
  {
    this.organizationList = new ArrayList();
  }
  
  public ArrayList<Organization> getOrganizationList()
  {
    return this.organizationList;
  }
  
  public Organization createOrganization(Organization.Type type)
  {
    Organization organization = null;
    if (type.getValue().equals(Organization.Type.TrashMonitor.getValue()))
    {
      organization = new TrashMonitorOrganization();
      this.organizationList.add(organization);
    }
    else if (type.getValue().equals(Organization.Type.FleetOperator.getValue()))
    {
      organization = new FleetOperatorOrganization();
      this.organizationList.add(organization);
    }
    else if (type.getValue().equals(Organization.Type.Driver.getValue()))
    {
      organization = new DriverOrganization();
      this.organizationList.add(organization);
    }
    else if (type.getValue().equals(Organization.Type.Technician.getValue()))
    {
      organization = new TechnicianOrganization();
      this.organizationList.add(organization);
    }
    else {
    }
    return organization;
  }
}

    

