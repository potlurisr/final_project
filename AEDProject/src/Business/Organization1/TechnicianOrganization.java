/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization1;

import Business.Role1.Role;
import Business.Role1.TechnicianRole;
import Business.Technician.TechnicianDirectory;
import java.util.ArrayList;

/**
 *
 * @author sriman
 */
public class TechnicianOrganization extends Organization {

     private TechnicianDirectory td;
     public TechnicianOrganization()
  {
    super(Organization.Type.Technician.getValue());
    td=new TechnicianDirectory();
  }
  
  public ArrayList<Role> getSupportedRole()
  {
      roles.add(new TechnicianRole());
    return roles;
  }

    public TechnicianDirectory getDd() {
        return td;
    }

    public void setDd(TechnicianDirectory dd) {
        this.td = dd;
    }
    
}
