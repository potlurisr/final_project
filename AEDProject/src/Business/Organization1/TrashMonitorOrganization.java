/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization1;

import Business.Role1.Role;
import Business.Role1.TrashMonitorRole;
import Business.Trash.TrashCan;
import java.util.ArrayList;

/**
 *
 * @author sriman
 */
public class TrashMonitorOrganization extends Organization {
    
    //a new arraylist to hold all the trashcans
    private ArrayList<TrashCan> tc;
    private ArrayList<Integer> sortedtc;//after calculating shortest path
    
    public TrashMonitorOrganization()
  {
    super(Organization.Type.TrashMonitor.getValue());
    tc=new ArrayList<>();
    sortedtc=new ArrayList<>();
    sortedtc.add(1);
        
    
  }

    public ArrayList<Integer> getSortedtc() {
        return sortedtc;
    }

    public void setSortedtc(ArrayList<Integer> sortedtc) {
        this.sortedtc = sortedtc;
    }
    
  
  public ArrayList<Role> getSupportedRole()
  {
      
        roles.add(new TrashMonitorRole());
        return roles;
    
  }

    public ArrayList<TrashCan> getTc() {
        return tc;
    }

    public void setTc(ArrayList<TrashCan> tc) {
        this.tc = tc;
    }
  
}

    

