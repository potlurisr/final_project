/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role1;

import Business.Business.EcoSystem;
import Business.Enterprise1.Enterprise;
import Business.Organization1.Organization;
import Business.UserAccount1.UserAccount;
import InterfaceEnterpriseAdminWorkArea.AdminWorkAreaJPanel;

import javax.swing.JPanel;

/**
 *
 * @author raunak
 */
public class AdminRole extends Role{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
        return new AdminWorkAreaJPanel(userProcessContainer, enterprise);
    }

    
    
}
