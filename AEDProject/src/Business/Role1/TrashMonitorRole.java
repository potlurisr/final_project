/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role1;

import Business.Business.EcoSystem;
import Business.Enterprise1.Enterprise;
import Business.Organization1.Organization;
import Business.Organization1.TrashMonitorOrganization;
import Business.UserAccount1.UserAccount;
import InterfaceTrashMonitorWorkArea.TrashMonitorWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author sriman
 */
public class TrashMonitorRole extends Role{
    
     @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount ua, Organization o, Enterprise enterprise, EcoSystem es) {
        return new TrashMonitorWorkAreaJPanel(userProcessContainer, ua, (TrashMonitorOrganization)o, enterprise); //To change body of generated methods, choose Tools | Templates.
    }
    
}
