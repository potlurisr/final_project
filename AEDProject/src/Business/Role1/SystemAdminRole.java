/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role1;

import Business.Business.EcoSystem;
import Business.Enterprise1.Enterprise;
import Business.Organization1.Organization;
import Business.UserAccount1.UserAccount;
import InterfaceSystemAdminWorkArea.SystemAdminWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author sriman
 */
public class SystemAdminRole extends Role {
    
    

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount ua, Organization o, Enterprise enterprise, EcoSystem es) {
        return new SystemAdminWorkAreaJPanel(userProcessContainer, es); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
