/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role1;

/**
 *
 * @author sriman
 */


import Business.Business.EcoSystem;
import Business.Enterprise1.Enterprise;
import Business.Organization1.Organization;
import Business.UserAccount1.UserAccount;
import javax.swing.JPanel;

public abstract class Role
{
  public abstract JPanel createWorkArea(JPanel paramJPanel, UserAccount paramUserAccount, Organization paramOrganization, Enterprise paramEnterprise, EcoSystem paramEcoSystem);
  
  public static enum RoleType
  {
    Admin("Admin"),  TrashMonitor("TrashMonitor"),  FleetOperator("FleetOperator"),Driver("Driver"),Technician("Technician");
    
    private String value;
    
    private RoleType(String value)
    {
      this.value = value;
    }
    
    public String getValue()
    {
      return this.value;
    }
    
    public String toString()
    {
      return this.value;
    }
  }
  
  public String toString()
  {
    return getClass().getName();
  }
}

