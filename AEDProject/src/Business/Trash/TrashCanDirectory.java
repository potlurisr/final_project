/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Trash;


import java.util.ArrayList;

/**
 *
 * @author sriman
 */
public class TrashCanDirectory {
    
    private ArrayList<TrashCan> d;
    
    public TrashCanDirectory()
    {
        d=new ArrayList<>();
    }
    
    public TrashCan addTrash()
    {
        // //creating like this to allow downcast an Employee  object to Donor object
        TrashCan e1=new TrashCan();
        d.add(e1);
        return e1;
    }

    public ArrayList<TrashCan> getD() {
        return d;
    }

    
    
    
    
}
