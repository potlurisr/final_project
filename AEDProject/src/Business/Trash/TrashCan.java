/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Trash;


/**
 *
 * @author sriman
 */
public class TrashCan  {
    
    private int id; 
    private int xcoordinate;
    private int ycoordinate;
    private String health;
    private String temperature;
    private String size;
    private int fill_level;
    private static int counter;

    public int getXcoordinate() {
        return xcoordinate;
    }

    public void setXcoordinate(int xcoordinate) {
        this.xcoordinate = xcoordinate;
    }

    public int getYcoordinate() {
        return ycoordinate;
    }

    public void setYcoordinate(int ycoordinate) {
        this.ycoordinate = ycoordinate;
    }

    public static int getCounter() {
        return counter;
    }

    public static void setCounter(int counter) {
        TrashCan.counter = counter;
    }
    

    public TrashCan()
    {
        id=counter;
        counter++;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHealth() {
        return health;
    }

    public void setHealth(String health) {
        this.health = health;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getFill_level() {
        return fill_level;
    }

    public void setFill_level(int fill_level) {
        this.fill_level = fill_level;
    }

   
    @Override
 public String toString()
    {
        return Integer.toString(id);
    }
    
    
   
    
    
    
    
    
    
}
