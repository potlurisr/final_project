/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Employee1;

/**
 *
 * @author sriman
 */


import java.util.ArrayList;

public class EmployeeDirectory
{
  private ArrayList<Employee> employeeList;
  
  public EmployeeDirectory()
  {
    this.employeeList = new ArrayList();
  }
  
  public ArrayList<Employee> getEmployeeList()
  {
    return this.employeeList;
  }
  
  public Employee createEmployee(String name)
  {
    Employee employee = new Employee();
    employee.setName(name);
    this.employeeList.add(employee);
    return employee;
  }
}
