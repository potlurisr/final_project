/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Vehicle;

/**
 *
 * @author sriman
 */
public class Vehicle {
    
    private String type;
    private String Companyname;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    private static int counter;
    
    public Vehicle()
    {
        id=counter;
        counter++;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCompanyname() {
        return Companyname;
    }

    public void setCompanyname(String Companyname) {
        this.Companyname = Companyname;
    }
    
   public String toString()
   {
       return Integer.toString(id);
   }
    
    
    
}
